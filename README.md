# rock paper scissors

The classic game, but now in your browser!

## Usage

[Click here](https://emerac.gitlab.io/rock-paper-scissors) to view the page
live!

Alternatively, you can clone this repository and then open the `index.html`
file with your browser.

## License

This project is licensed under the GNU General Public License.
For the full license text, view the [LICENSE](LICENSE) file.
