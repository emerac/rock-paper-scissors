/* Play a game of rock paper scissors.
 *
 * Copyright (C) 2021 emerac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
const choices = document.querySelectorAll('.choices')
for (let i = 0; i <= choices.length - 1; i++) {
    choices[i].addEventListener('click', e => {
        playGame(convertToSentenceCase(e.target.textContent))
    });
}
const container = document.querySelector('.main-container');
const resultDisplay = document.createElement('p');
resultDisplay.classList.add('result');
container.appendChild(resultDisplay);
const scoreDisplay = document.createElement('p');
scoreDisplay.classList.add('score');
container.appendChild(scoreDisplay);
const winnerDisplay = document.createElement('p');
winnerDisplay.classList.add('winner');
container.appendChild(winnerDisplay);
const infoDisplay = document.createElement('p');
infoDisplay.classList.add('info');
container.appendChild(infoDisplay);


function convertToSentenceCase(a_string) {
    return a_string.charAt(0).toUpperCase() + a_string.toLowerCase().substr(1);
}


function getComputerMove() {
    let actionNumber = Math.floor(Math.random() * 3);
    switch (actionNumber) {
        case 0:
            return "Rock";
        case 1:
            return "Paper";
        case 2:
            return "Scissors";
        default:
            throw new Error("Unreachable.");
    }
}


function playGame(playerMove) {
    const resultDisplay = document.querySelector('.result');
    const scoreDisplay = document.querySelector('.score');
    const winnerDisplay = document.querySelector('.winner');
    const infoDisplay = document.querySelector('.info');


    if (scoreDisplay.textContent.length === 0) {
        scoreDisplay.textContent = "0 - 0 - 0";
    }
    score = scoreDisplay.textContent.split(" - ");
    let playerWins = Number(score[0]);
    let computerWins = Number(score[1]);
    let ties = Number(score[2]);

    let totalGames = playerWins + computerWins + ties;
    if (totalGames >= 10) {
        playerWins = 0;
        computerWins = 0;
        ties = 0;
        winnerDisplay.textContent = "";
        infoDisplay.textContent = "";
    }

    roundResult = playRound(playerMove, getComputerMove());
    if (roundResult === "player") {
        resultDisplay.textContent = "You won the round!";
        playerWins++;
    } else if (roundResult === "computer") {
        resultDisplay.textContent = "You lost the round!";
        computerWins++;
    } else {
        resultDisplay.textContent = "The round was a tie!";
        ties++;
    }

    score[0] = playerWins;
    score[1] = computerWins;
    score[2] = ties;
    scoreDisplay.textContent = score.join(" - ");

    totalGames = playerWins + computerWins + ties;
    if (totalGames >= 10) {
        if (playerWins > computerWins && playerWins > ties) {
            winnerDisplay.textContent = "Congratulations, you won the game!";
        } else if (computerWins > playerWins && computerWins > ties) {
            winnerDisplay.textContent = "Sorry, the computer won the game.";
        } else {
            winnerDisplay.textContent = "The game was a tie!";
        }
        infoDisplay.textContent = "(choose a move to play again)";
    }
}


function playRound(playerMove, computerMove) {
    let formattedPlayerMove = convertToSentenceCase(playerMove);
    if (formattedPlayerMove === "Rock") {
        switch (computerMove) {
            case "Rock":
                return "tie";
            case "Paper":
                return "computer";
            case "Scissors":
                return "player";
            default:
                throw new Error("Unreachable.");
        }
    } else if (formattedPlayerMove === "Paper") {
        switch (computerMove) {
            case "Rock":
                return "player";
            case "Paper":
                return "tie";
            case "Scissors":
                return "computer";
            default:
                throw new Error("Unreachable.");
        }
    } else {
        switch (computerMove) {
            case "Rock":
                return "computer";
            case "Paper":
                return "player";
            case "Scissors":
                return "tie";
            default:
                throw new Error("Unreachable.");
        }
    }
}
